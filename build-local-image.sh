
OAUTH2_ESTENSION_VERSION=2.0.0

curl -L https://gitlab.com/william.younang/hivemq-oauth2-rbac-extension/-/archive/${OAUTH2_ESTENSION_VERSION}/hivemq-oauth2-rbac-extension-${OAUTH2_ESTENSION_VERSION}.zip -o hivemq-oauth2-rbac-extension-${OAUTH2_ESTENSION_VERSION}.zip

unzip hivemq-oauth2-rbac-extension-${OAUTH2_ESTENSION_VERSION}.zip

rm hivemq-oauth2-rbac-extension-${OAUTH2_ESTENSION_VERSION}.zip

cd hivemq-oauth2-rbac-extension-${OAUTH2_ESTENSION_VERSION}

mvn clean package -DskipTests

mv target/hivemq-oauth2-rbac-extension-${OAUTH2_ESTENSION_VERSION}-distribution.zip ../hivemq-oauth2-rbac-extension-${OAUTH2_ESTENSION_VERSION}-distribution.zip

cd .. 

rm -r hivemq-oauth2-rbac-extension-${OAUTH2_ESTENSION_VERSION}

docker build -t younanglab/hivemq:${OAUTH2_ESTENSION_VERSION} .

rm hivemq-oauth2-rbac-extension-${OAUTH2_ESTENSION_VERSION}-distribution.zip

# Remove dangling images
docker rmi -f $(docker images -q -f dangling=true)
