FROM openjdk:11-jre-slim

LABEL maintainer="William YOUNANG"

ARG HIVEMQ_CE_VERSION=2020.4
ARG HIVEMQ_GID=10000
ARG HIVEMQ_UID=10000

ARG PROMOTHEUS_ESTENSION_VERSION=4.0.3
ARG OAUTH2_ESTENSION_VERSION=2.0.0

# Additional JVM options, may be overwritten by user
ENV JAVA_OPTS "-XX:+UseNUMA"

ENV RBAC_CONFIG_RELOAD_INTERVAL "70"
ENV AUTH2_SERVER_URL "http://192.168.1.53:8080/auth"
ENV AUTH2_CLIENT_ID "human-clinic"
ENV AUTH2_REALM_ID "a valid realm"
ENV COUCHBASE_HOSTS "someIpAddress"
ENV COUCHBASE_USERNAME "hivemq-rbac"
ENV COUCHBASE_PASSWORD "ChangeThisBeforeProd"
ENV COUCHBASE_BUCKET "hivemq_oauth2_rbac_config"
ENV COUCHBASE_ENABLE_SSL "false"
ENV COUCHBASE_KEYSTORE_FILE ""
ENV COUCHBASE_KEYSTORE_PASSWORD ""
ENV COUCHBASE_TRUSTSTORE_FILE ""
ENV COUCHBASE_TRUSTSTORE_PASSWORD ""

# We use tini for proper signal propagation and a clean init
ENV TINI_VERSION v0.18.0

RUN set -x \
    && apt-get update && apt-get install -y --no-install-recommends curl gnupg gnupg-agent dirmngr unzip \
    && curl -fSL "https://github.com/krallin/tini/releases/download/$TINI_VERSION/tini" -o /usr/local/bin/tini \
    && curl -fSL "https://github.com/krallin/tini/releases/download/$TINI_VERSION/tini.asc" -o /usr/local/bin/tini.asc \
    && export GNUPGHOME="$(mktemp -d)" \
    && echo "disable-ipv6" >> "$GNUPGHOME"/dirmngr.conf \
    && gpg --no-tty --keyserver ha.pool.sks-keyservers.net --recv-keys 6380DC428747F6C393FEACA59A84159D7001A4E5 \
    && gpg --batch --verify /usr/local/bin/tini.asc /usr/local/bin/tini \
    && rm -rf "$GNUPGHOME" /usr/local/bin/tini.asc \
    && chmod +x /usr/local/bin/tini \
    && apt-get remove -y gnupg gnupg-agent dirmngr && rm -rf /var/lib/apt/lists/*

COPY config.xml /opt/config.xml

# HiveMQ setup
RUN curl -L https://github.com/hivemq/hivemq-community-edition/releases/download/${HIVEMQ_CE_VERSION}/hivemq-ce-${HIVEMQ_CE_VERSION}.zip -o /opt/hivemq-${HIVEMQ_CE_VERSION}.zip \
    && unzip /opt/hivemq-${HIVEMQ_CE_VERSION}.zip  -d /opt/\
    && rm -f /opt/hivemq-${HIVEMQ_CE_VERSION}.zip \
    && ln -s /opt/hivemq-ce-${HIVEMQ_CE_VERSION} /opt/hivemq \
    && mv /opt/config.xml /opt/hivemq-ce-${HIVEMQ_CE_VERSION}/conf/config.xml \
    && groupadd --gid ${HIVEMQ_GID} hivemq \
    && useradd -g hivemq -d /opt/hivemq -s /bin/bash --uid ${HIVEMQ_UID} hivemq \
    && chown  hivemq:hivemq /opt/hivemq-ce-${HIVEMQ_CE_VERSION} \
    && chmod -R 0777 /opt \
    && chmod +x /opt/hivemq/bin/run.sh

#clean up extension folder to remove extensions
RUN rm -r /opt/hivemq/extensions/*

# prometheus plugin setup.
RUN curl -L https://github.com/hivemq/hivemq-prometheus-extension/releases/download/${PROMOTHEUS_ESTENSION_VERSION}/hivemq-prometheus-extension-${PROMOTHEUS_ESTENSION_VERSION}-distribution.zip -o /opt/hivemq/extensions/prometheus-extension.zip \
    && unzip /opt/hivemq/extensions/prometheus-extension.zip  -d /opt/hivemq/extensions/\
    && rm -f /opt/hivemq/extensions/prometheus-extension.zip

# oauth 2 plugin setup
COPY hivemq-oauth2-rbac-extension-${OAUTH2_ESTENSION_VERSION}-distribution.zip /opt/hivemq/extensions/oauth2-extension.zip
RUN unzip /opt/hivemq/extensions/oauth2-extension.zip  -d /opt/hivemq/extensions/\
    && rm -f /opt/hivemq/extensions/oauth2-extension.zip

# Substitute eval for exec and replace OOM flag if necessary (for older releases). This is necessary for proper signal propagation
RUN sed -i -e 's|eval \\"java\\" "$HOME_OPT" "$JAVA_OPTS" -jar "$JAR_PATH"|exec "java" $HOME_OPT $JAVA_OPTS -jar "$JAR_PATH"|' /opt/hivemq/bin/run.sh && \
    sed -i -e "s|-XX:OnOutOfMemoryError='sleep 5; kill -9 %p'|-XX:+CrashOnOutOfMemoryError|" /opt/hivemq/bin/run.sh

# Make broker data persistent throughout stop/start cycles
VOLUME /opt/hivemq/data

# Persist log data
VOLUME /opt/hivemq/log

# Allow configuration modification from volume
VOLUME /opt/hivemq/conf


# 9399 exposed port for prometheus estension
# 1883 exposed port for mqtt
# 1884 exposed port for web socket mqtt
EXPOSE 1883 1884 9399

WORKDIR /opt/hivemq
USER ${HIVEMQ_UID}

ENTRYPOINT ["/usr/local/bin/tini", "-g", "--"]
CMD ["/opt/hivemq/bin/run.sh"]
