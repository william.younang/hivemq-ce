Here we provide a dockerized version of the HiveMQ CE broker with some built-in extensions such as :

## Prometheus Extension
This extension is provided here with the default parameters. The port 9399 is exposed and can be binded with any available port. It is mandatory to have a running instance of Prometheus. Additional info are available at https://github.com/hivemq/hivemq-prometheus-extension

## OAuth2 RBAC Extension
Detailed description of this security extension is available at https://gitlab.com/william.younang/hivemq-oauth2-rbac-extension

# Building the image locally

1) Install Docker and Docker Compose <br/>
2) run the script  build-local-image.sh <br/>

# Start a basic local instance

1) If there are no modification on the docker-compose yaml file, make sure that the docker container has write access to the folder used as volume. <br/>
   
   
# Static Cluster Mode

In order to have a static cluster, the file config-static-cluster.xml can be used as a starting point. Additional info here (https://www.hivemq.com/docs/4.2/hivemq/cluster.html).


# TODO

1) Create ready to use image on dockerhub